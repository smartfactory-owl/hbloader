# HBloader

HBloader is an extended hawkBit client that installs and manages rolled out apps as Docker containers.
Installed Apps can be monitored, restarted and deleted via a web interface.

## Installation and configuration

    git pull https://gitlab.com/smartfactory-owl/hbloader
    rename hblcfg_sample.json to hblcfg.json 
    modify hblcfg.json (hawkBit credentials and device config)
    sudo pip3 install -r requirements.txt
    sudo python3 hbloader.py
    go to hawkBit Management-UI and deploy apps

    The initial configuration can be modified via web interface

## Web interface
The Web interface is available at http://localhost:8081.

## Docker
Download Docker image
``` Shell
sudo docker pull registry.gitlab.com/smartfactory-owl/hbloader:latest
```

Run Docker container with
```
docker run -d --name <container-name> --net=host -v /var/run/docker.sock:/var/run/docker.sock -v /home/root/.docker/:/root/.docker/ registry.gitlab.com/smartfactory-owl/hbloader:latest
```

Optional ENVs for HawkBti configuration:
```
--env IP_ADDRESS=<hawkbit ip-address>
--env PORT=<hawkbit port>
--env USE_SSL=<True/False>
--env TENANT_ID=<tenant_id>
--env TARGET_NAME=<target_name>
--env USER=<hb_user>
--env PASSWORD=<hb_password>
--env LOG_LEVEL=<log_level>
--env CONTROLLER_ID=<controller_id>
--env DOCKER_USER=<user>
--env DOCKER_PASSWD=<password>
--env DOCKER_EMAIL=<email>
--env DOCKER_REGISTRY=<registry>
--env SINGLE_APP_MODE=<True/False>
```


**License**: LGPLv2.1

Copyright
---------

    Copyright (C) Additional code Eugene Nuribekov, Jan Alsters, Patrick Heidemann

Software based on rauc-hawkbit
https://github.com/rauc/rauc-hawkbit

    Copyright (C) 2016-2020 Pengutronix, Enrico Joerns <entwicklung@pengutronix.de>
    Copyright (C) 2016-2020 Pengutronix, Bastian Stender <entwicklung@pengutronix.de>
    
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.
    
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA



