import asyncio
import docker
import json
from docker.types import LogConfig
import tarfile
from pathlib import Path
import subprocess
import re
import socket
import os
from aiohttp.client_exceptions import ClientOSError, ClientResponseError
from datetime import datetime, timedelta

from .ddi.client import DDIClient, APIError
from .ddi.client import (ConfigStatusExecution, ConfigStatusResult)
from .ddi.deployment_base import (
    DeploymentStatusExecution, DeploymentStatusResult)
from .ddi.cancel_action import (
    CancelStatusExecution, CancelStatusResult)
from .mi.client import MIClient
import logging


class HBClient(object):
    """
    HawkBit downloader
    """

    def __init__(self,
                 session,
                 result_callback,
                 step_callback=None,
                 lock_keeper=None,
                 **kwargs):

        super(HBClient, self).__init__()

        self.logger = logging.getLogger('hbloader')
        self.session = session

        self.docker_client = None

        self.config = kwargs

        self.logger.debug(self.config)

        self.run_mode = kwargs['run_as_service']
        self.docker_mode = True
        self.attributes = kwargs['attributes']

        self.action_id = None
        self.lock_keeper = lock_keeper

        self.result_callback = result_callback
        self.step_callback = step_callback

        self.dl_dir = Path.joinpath(Path.home(), 'BUNDLE')
        Path(self.dl_dir).mkdir(parents=True, exist_ok=True)

        self.dl_filename = ''
        self.service_dir = Path.joinpath(Path.home(), '.config/systemd/user')
        Path(self.service_dir).mkdir(parents=True, exist_ok=True)
        self.auth_token = ''
        self.controller_id = kwargs['controller_id']
        self.mi = MIClient(session, **kwargs)
        self.ddi = None
        self.single_app_mode = kwargs['single_app_mode']
        #hbloader short container id = hostname
        self.container_id = socket.gethostname()

    def get_used_ports(self):
        """
        returns all ports used by docker containers
        """
        containers = self.docker_client.containers.list(all=True)
        ports = []
        for container in containers:
            for key in container.ports:
                plist = container.ports[key]
                if plist:
                    for hp in plist:
                        ports.append(hp.get('HostPort'))
        return ports

    def is_port_in_use(self, port):
        """
        trys to connect to the given port
        """
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.bind(("127.0.0.1", port))
        except socket.error:
            s.close()
            return True
        s.close()
        return False

    def portcheck(self, port):
        """
        checks if port is in use
        increments if that's the case
        """
        if self.is_port_in_use(port):
            self.logger.debug('The port {} is already in use. Incrementing.'.format(port))
            new_port = port + 1
            return self.portcheck(new_port)
        return port

    def check_container_name(self, name):
        '''
        change container name if already exists
        for example: test-container -> test-container_2
        '''
        count = 1
        while name in self.container_names:
            count+=1
            name = name + '_' + str(count)
        return name

    def delete_containers(self):
        """
        stops and deletes all containers
        except hbloader container
        """
        self.docker_client = docker.from_env()
        self.logger.debug('Stopping running containers...')
        containers = self.docker_client.containers.list(all=True)
        for container in containers:
            #cnt = self.docker_client.containers.get(container.short_id)
            if (container.short_id != self.container_id) and ("hbloader" not in container.name):
                try:
                    container.stop()
                    #wait until container is stopped
                    container.wait()
                    #delete container
                    container.remove()
                except:
                    self.logger.error('Failed to stop container {}'.format(container.name))
    async def run_ddi(self):
        '''
        Register target on server using MI
        and switch to DDI with received parmeters
        '''
        self.logger.info('Register target on server...')

        ''' loop until target will be registred on server'''
        while True:
            target = await self.get_target_details()
            if target:
                break
            await self.mi.register_target()

        self.logger.info('Target {} succeessfully registered.'.format(target['name']))
        self.logger.debug('name: \n {}'.format(target['name']))
        self.logger.debug('controller_id: \n {}'.format(target['controllerId']))
        self.logger.debug('securityTocken: \n {}'.format(target['securityToken']))

        '''
        get generated on server auth tocken,
        add it to config dictionary
        and run DDI with full set of params
        '''
        self.config['auth_token'] = target['securityToken']
        self.logger.debug('{}'.format(self.config))
        self.ddi = DDIClient(self.session, **self.config)

    async def get_target_details(self):
        '''
        If target exists return details (dict)
        If not return None
        '''
        #self.logger.info('')

        targets = await self.mi()
        content = targets['content']
        self.logger.debug(content)
        if content:
            for item in content:
                self.logger.debug('content item: {}'.format(item))
                if item['controllerId'] == self.controller_id:
                    return item
        self.logger.debug('no content')
        return None

    async def start_polling(self, wait_on_error=60):
        """
        Wrapper around self.poll_base_resource() for exception handling.
        """
        self.logger.info('Polling started.')

        INFO_POLLING = 'Polling cancelled'
        WARN_TIMEOUT = 'Polling failed due to TimeoutError'
        WARN_TEMP_ERROR = 'Polling failed with a temporary error:'
        WARN_EXCEPTION = 'Polling failed with an unexpected exception:'
        INFO_RETRY_FMT = 'Retry will happen in {} seconds'

        while True:
            try:
                await self.poll_base_resource()
            except asyncio.CancelledError:
                self.logger.debug(INFO_POLLING)
                break

            except asyncio.TimeoutError:
                self.logger.warning(WARN_TIMEOUT)

            except (APIError,
                    TimeoutError,
                    ClientOSError,
                    ClientResponseError) as e:
                # log error and start all over again
                self.logger.warning('{} {}'.format(WARN_TEMP_ERROR, e))

            except Exception:
                self.logger.exception(WARN_EXCEPTION)

            self.action_id = None
            self.logger.debug(INFO_RETRY_FMT.format(wait_on_error))

            await asyncio.sleep(wait_on_error)

    async def poll_base_resource(self):
        """
        Poll DDI API base resource.
        """
        while True:

            base = await self.ddi()
            if '_links' in base:

                if 'configData' in base['_links']:
                    await self.identify(base)

                if 'deploymentBase' in base['_links']:
                    await self.process_deployment(base)

                if 'cancelAction' in base['_links']:
                    #await self.cancel(base)
                    pass

            await self.sleep(base)

    async def identify(self, base):
        """
        Identify target against HawkBit.
        """
        self.logger.debug('Identify target against Hawkbit...')

        await self.ddi.configData(
                ConfigStatusExecution.closed,
                ConfigStatusResult.success, **self.attributes)

    async def process_deployment(self, base):
        """
        Check and download deployments
        """
        self.logger.debug('> process_deployment')

        '''disabled action_id check to enable multiple deployment of the same distro''' 
        #if self.action_id is not None:
        #    self.logger.info('Deployment is already in progress')
        #    return

        # retrieve action id and resource parameter from URL
        deployment = base['_links']['deploymentBase']['href']
        self.logger.debug('deploymentBase: {}'.format(deployment))
        match = re.search('/deploymentBase/(.+)\?c=(.+)$', deployment)
        action_id, resource = match.groups()
        self.logger.debug('action_id: {}'.format(action_id))
        self.logger.debug('resource: {}'.format(resource))
        self.logger.info('Deployment found for this target')
        # fetch deployment information
        deploy_info = await self.ddi.deploymentBase[action_id](resource)

        #get list of apps (chunks = apps)
        apps = deploy_info['deployment']['chunks']
        apps_total = len(apps)
        #delete all running containers in single app mode
        if self.single_app_mode:
            self.delete_containers()
        for i in range(1, apps_total + 1):
            #process deployment for each app
            self.logger.info("Installing App {0} of {1}".format(i, apps_total))
            try:
                app = deploy_info['deployment']['chunks'][i-1]
            except IndexError:
                # send negative feedback to HawkBit
                status_execution = DeploymentStatusExecution.closed
                status_result = DeploymentStatusResult.failure
                msg = 'Deployment without chunks found. Ignoring'
                await self.ddi.deploymentBase[action_id].feedback(
                        status_execution, status_result, [msg])
                raise APIError(msg)

            try:
                artifact = app['artifacts'][0]
                self.logger.debug('artifact: {}'.format(artifact))

            except IndexError:
                # send negative feedback to HawkBit
                status_execution = DeploymentStatusExecution.closed
                status_result = DeploymentStatusResult.failure
                msg = 'Deployment without artifacts found. Ignoring'

            # prefer https ('download') over http ('download-http')
            # HawkBit provides either only https, only http or both
            if 'download' in artifact['_links']:
                download_url = artifact['_links']['download']['href']
                self.logger.debug('download url: {}'.format(download_url))

            else:
                download_url = artifact['_links']['download-http']['href']
                self.logger.debug('download url: {}'.format(download_url))

            # download artifact, check md5 and report feedback
            md5_hash = artifact['hashes']['md5']
            self.logger.info('Downloading bundle...')
            await self.download_artifact(action_id, download_url, md5_hash)

            # download successful, start install
            self.logger.info('Starting installation...')
            try:
                self.action_id = action_id
                #await asyncio.shield(self.install())
                await self.install()
            except Exception as e:
                # send negative feedback to HawkBit
                status_execution = DeploymentStatusExecution.closed
                status_result = DeploymentStatusResult.failure
                await self.ddi.deploymentBase[action_id].feedback(
                        status_execution, status_result, [str(e)])
                raise APIError(str(e))
        
        status_execution = DeploymentStatusExecution.closed
        status_result = DeploymentStatusResult.success
        #call install complete
        await self.ddi.deploymentBase[self.action_id].feedback(
                status_execution, status_result, ['Install completed'])

    async def install(self):
        
        #self.logger.debug('{} {}'.format(self.dl_dir, self.dl_filename))
        manifest_file_name = Path(self.dl_dir).joinpath(self.dl_filename)
        manifest = {}
        restart_policies = ['always', 'on-failure' ]
        network_modes = ['bridge', 'host', 'overlay', 'macvlan', 'none']

        with open(manifest_file_name, "r") as manifest_file:
            manifest = json.load(manifest_file) 

        s_type = manifest["typeDetail"]
        
        #DOCKER
        if s_type == 'docker':
            self.docker_client = docker.from_env()
            
            #parse optional docker credentials for login
            docker_username = os.environ.get('DOCKER_USER')
            docker_passwd = os.environ.get('DOCKER_PASSWD')
            docker_email = os.environ.get('DOCKER_EMAIL')
            docker_registry = self.config['docker_registry']

            try:
                self.docker_client.login(username=docker_username, password=docker_passwd, email=docker_email, registry=docker_registry)
            except:
                self.logger.error("Docker login failed!")
            
            options = manifest["containerCreateOptions"]
            port_bindings = options["HostConfig"]["PortBindings"]
            uri = manifest["imageUri"]
            try:
                volumes = options["HostConfig"]["Binds"]
            except:
                volumes = []
            try:
                devices = options["HostConfig"]["Devices"]
            except:
                devices = []
            try:
                network_mode = options["HostConfig"]["NetworkMode"]
            except:
                network_mode = 'bridge'
            if network_mode not in network_modes:
                network_mode = 'bridge'
            restart_policy = options["HostConfig"]["RestartPolicy"]
            if restart_policy not in restart_policies:
                restart_policy = {"Name": "always"}
            else:
                restart_policy = {"Name": restart_policy}
            try:
                privileged = options["HostConfig"]["Privileged"]
                if privileged:
                    privileged = True
                else:
                    privileged = False
            except:
                privileged = False
            try:
                envs = manifest["environmentVariables"]
            except:
                envs = []
            #get container names
            containers = self.docker_client.containers.list(all=True)
            self.container_names = []
            for container in containers:
                self.container_names.append(container.name)
            container_name = manifest["containerName"]
            if self.container_names:
                container_name = self.check_container_name(container_name)

            ports = {}
            #check ports
            for port_int, port_list in port_bindings.items():
                hostports = []
                for entry in port_list:
                    hostport = self.portcheck(int(entry["HostPort"]))
                    hostports.append(hostport)
                ports[port_int] = hostports
        
            self.logger.info("Downloading docker image...")
            self.docker_client.images.pull(uri, tag=manifest["imageVersion"])

            self.logger.debug("Image download finished.")
            #self.logger.info("Images available:\n{}".format(images))

            await self.process_image(uri, container_name, ports, privileged, volumes, devices, network_mode, restart_policy, envs)
        
        elif s_type == 'delete':
            self.docker_client = docker.from_env()
            containers = self.docker_client.containers.list(all=True)
            print(containers)
            container_name = manifest["containerName"]
            container_names = []
            for container in containers:
                container_names.append(container.name)
            print(container_names)
            #check if name is already in use
            if container_name in container_names:
                cnt = self.docker_client.containers.get(container_name)
                cnt.stop()
                #wait until container is stopped
                cnt.wait()
                #delete container
                cnt.remove()
                self.logger.info("{} successfully deleted!".format(container_name))
            else:
                self.logger.info("No container found with name {}!".format(container_name))

        #NODE-RED flow
        elif s_type == 'flow':
            pass
        #CONFIG
        elif s_type == 'config':
            pass
        #RAUC
        elif s_type == 'rauc':
            #rauc systemupdate
            pass
        #CERTIFICATE
        elif s_type == 'cert':
            #update certificate
            pass
        #SOFTWARE
        elif s_type == 'apt':
            #install new software via apt
            pass
    
    async def update_rauc(self):
        pass
    
    async def update_cert(self):
        pass

    async def install_software(self):
        pass

    async def process_image(self, image, name, ports, privileged, volumes, devices, networkmode, restart_policy, envs):
        '''
        Make descision about image usage
        and run container if yes.
        '''
        #self.logger.info('')
        if self.docker_mode == 'no':
            self.logger.error("no start container")
            return

        self.logger.info("Starting container...")
        log_params = {'max-size': '10m', 'max-file': '3'}
        log_config = LogConfig(type=LogConfig.types.JSON, config=log_params)

        #start container with given parameters
        container = self.docker_client.containers.run(image,
                                        name=name,
                                        detach=True,
                                        log_config=log_config, 
                                        ports=ports,
                                        privileged=privileged,
                                        volumes=volumes,
                                        devices=devices,
                                        network_mode=networkmode,
                                        restart_policy=restart_policy,
                                        environment=envs)

        self.logger.debug('Container {} {} {}'.format(container.short_id, container.name, container.status))

    async def run_as_service(self):
        '''
        If it enabled by configuration
        create service file and pass  it to systemd.
        '''

        self.logger.info('> run_as_service {} {}'.format(self.run_mode, self.dl_filename))

        '''
        choose operating mode
        '''
        if self.run_mode == 'no':
            return

        if self.run_mode == 'ask':
            print("Run installed as service ?")
            #if not self.ask_yn():
            #    return
        '''
        create service file, put it to systemd
        '''
        app_name =  self.dl_filename.split('-')[0]
        service_file_name = app_name + '.service'
        exec_file_name = app_name + '.py'

        self.logger.debug('Names: {} {} {}'.format(app_name, service_file_name, exec_file_name))

        self.create_service_file(service_file_name, exec_file_name)

        self.logger.debug('service_dir {}'.format(self.service_dir))

        commands = [['pwd'],
                    ['ls', '-la'],
                    ['cp', service_file_name, self.service_dir],
                    ['systemctl', '--user', 'daemon-reload'],
                    ['systemctl', '--user', 'enable', service_file_name],
                    ['systemctl', '--user', 'start', service_file_name],
                    ['systemctl', '--user', 'status', service_file_name]]

        for command in commands:
            self.logger.info(command)
            process = subprocess.run(command, cwd=self.dl_dir)
            rc = process.returncode

    async def download_artifact(self, action_id, url, md5sum,
                                tries=3):
        """
        Download bundle artifact.
        """
        #self.logger.info('')

        ERR_CHECKSUMM_FMT = 'Checksum does not match. {} tries remaining'
        STATUS_MSG_FMT = 'Artifact checksum does not match after {} tries.'

        try:
            match = re.search('/softwaremodules/(.+)/artifacts/(.+)$', url)
            software_module, self.dl_filename = match.groups()
            static_api_url = False

        except AttributeError:
            static_api_url = True

        if self.step_callback:
            self.step_callback(0, "Downloading bundle...")
        
        self.dl_filename = 'manifest.json'

        self.logger.debug('dl_filename: {}'.format(self.dl_filename))
        self.logger.debug('dl_dir: {}'.format(self.dl_dir))

        dl_location = Path(self.dl_dir).joinpath(self.dl_filename)

        # try several times
        for dl_try in range(tries):

            if not static_api_url:
                checksum = await self.ddi.softwaremodules[software_module].artifacts[self.dl_filename](dl_location)

            else:
                # API implementations might return static URLs, so bypass API
                # methods and download bundle anyway
                checksum = await self.ddi.get_binary(url, dl_location)

            if checksum == md5sum:
                self.logger.debug('Download successful')
                return

            else:
                self.logger.error(ERR_CHECKSUMM_FMT.format(tries-dl_try))

        # MD5 comparison unsuccessful, send negative feedback to HawkBit
        status_msg = STATUS_MSG_FMT.format(tries)
        status_execution = DeploymentStatusExecution.closed
        status_result = DeploymentStatusResult.failure

        self.logger.warning('Feedback failure')
        await self.ddi.deploymentBase[action_id].feedback(
                status_execution, status_result, [status_msg])

        raise APIError(status_msg)

    def identify_artifact(self):
        '''
        Get archive's file list and determines type of content.
        '''
        #self.logger.info('')

        dl_location = Path(self.dl_dir).joinpath(self.dl_filename)

        with tarfile.open(dl_location,'r') as tar:
            names = tar.getnames()

        items = [Path(name).name for name in names]

        if 'manifest.json' in items:
            result = 'docker'

        if 'setup.py' in items:
            result = 'python'

        self.logger.debug('{} is a |{}|'.format(self.dl_filename, result))
        return result

    async def sleep(self, base):
        """
        Sleep time
        """
        sleep_time = 10
        self.logger.info('Will sleep for {} seconds'.format(sleep_time))
        await asyncio.sleep(sleep_time)

    def create_service_file(self,
                            service_file_name,
                            exec_name,
                            description='hbloader test service'):
        '''
        Create .service file for installed programm.

        '''
        self.logger.info('> create_service_file {}'.format(self.run_mode))

        result = subprocess.run(['whereis', exec_name],  stdout=subprocess.PIPE)
        full_exec_name = result.stdout.split()[1].decode('utf-8')
        str = '''[Unit]
Description={}

[Service]
ExecStart=/usr/bin/python3 {}

[Install]
WantedBy=multi-user.target
'''.format(description, full_exec_name)
        p = Path.joinpath(self.dl_dir, service_file_name)
        with p.open( 'w') as service_file:
            service_file.write(str)
