from aiohttp import web
import jinja2
import aiohttp_jinja2
import aiohttp_session
import os
import json
import docker
import pathlib

class Hbloader_gui(object):

    def __init__(self):
        pass

    """
    Setting up routes
    """
    async def dashboard(self, request):
        #get all running containers
        try:
            docker_client = docker.from_env()
            container_list = docker_client.containers.list(all=True)
            containers = []
            for container in container_list:
                containers.append({'id': container.short_id, 'name': container.name, 'image': str(container.image)[9:-2], 'status': container.status, 'ports': container.ports})
        except:
            print("No Docker env found!")
            containers = []
        context = {
            'rstate': 1, 
            'containers': containers
        }
        response = aiohttp_jinja2.render_template("dashboard.html", request, context=context)

        return response

    async def logging(self, request):
        with open("hbloader.log", encoding="UTF-8") as log_file:
            context = {
                'timestamp': "2021-01-04",
                'log': log_file.read()
            }
        response = aiohttp_jinja2.render_template("logging.html", request, context=context)

        return response

    async def get_config(self, request):
        with open("hblcfg.json") as json_file: 
            context = json.load(json_file)
        response = aiohttp_jinja2.render_template("config.html", request, context=context)

        return response

    async def post_config(self, request):
        data = await request.post()
        tenent_id = data["tenentid"]
        user_id = data["userid"]
        passwd = data["passwd"]
        try:
            ssl_enabled = data["sslenabled"]
            if ssl_enabled:
                ssl_enabled = True
            else:
                ssl_enabled = False
        except:
            ssl_enabled = False
        host = data["host"]
        target_name = data["targetname"]
        controller_id = data["controllerid"]
        log_level = data["loglvl"]
        try:
            run_as_service = data["runasservice"]
            if run_as_service:
                run_as_service = "yes" 
            else:
                run_as_service = "no"
        except:
            run_as_service = "no"
        port = data["port"]
        docker_registry = data["docker_registry"]
        single_app_mode = data["sam"]
        #print(tenent_id, user_id, passwd, ssl_enabled, host, target_name, controller_id, log_level, run_as_service, port)
        newconfig = {}
        newconfig['ssl'] = ssl_enabled
        newconfig['host'] = host
        newconfig['tenant_id'] = tenent_id
        newconfig['target_name'] = target_name
        newconfig['login'] = user_id
        newconfig['password'] = passwd
        newconfig['auth_token'] = ""
        newconfig['attributes'] = {"MAC": ""}
        newconfig['loglevel'] = log_level
        newconfig['run_as_service'] = run_as_service
        newconfig['port'] = port
        newconfig['ip'] = host
        newconfig['controller_id'] = controller_id
        newconfig['docker_registry'] = docker_registry
        newconfig['single_app_mode'] = single_app_mode

        with open("hblcfg.json", 'w') as outfile:
            json.dump(newconfig, outfile, indent=4)
        print("Config wurde gespeichert!")

        with open("hblcfg.json") as json_file: 
            context = json.load(json_file)
        response = aiohttp_jinja2.render_template("config.html", request, context=context)


        return response

    async def stopContainer(self, request):
        #data = await request.post()
        #container_id = data['container_id']
        params = request.rel_url.query
        container_id = params['container_id']
        docker_client = docker.from_env()
        cnt = docker_client.containers.get(container_id)
        cnt.stop()
        #wait until container is stopped
        cnt.wait()
        
        container_list = docker_client.containers.list(all=True)
        containers = []
        for container in container_list:
            containers.append({'id': container.short_id, 'name': container.name, 'image': str(container.image)[9:-2], 'status': container.status, 'ports': container.ports})
        context = {
            'rstate': 1, 
            'containers': containers
        }
        response = aiohttp_jinja2.render_template("dashboard.html", request, context=context)

        return response

    async def startContainer(self, request):
        params = request.rel_url.query
        container_id = params['container_id']
        docker_client = docker.from_env()
        cnt = docker_client.containers.get(container_id)
        cnt.start()
        #wait until container is started
        cnt.wait()
        
        container_list = docker_client.containers.list(all=True)
        containers = []
        for container in container_list:
            containers.append({'id': container.short_id, 'name': container.name, 'image': str(container.image)[9:-2], 'status': container.status, 'ports': container.ports})
        context = {
            'rstate': 1, 
            'containers': containers
        }
        response = aiohttp_jinja2.render_template("dashboard.html", request, context=context)

        return response
    async def deleteContainer(self, request):
        params = request.rel_url.query
        container_id = params['container_id']
        docker_client = docker.from_env()
        cnt = docker_client.containers.get(container_id)
        cnt.remove()
        #wait until container is deleted

        container_list = docker_client.containers.list(all=True)
        containers = []
        for container in container_list:
            containers.append({'id': container.short_id, 'name': container.name, 'image': str(container.image)[9:-2], 'status': container.status, 'ports': container.ports})
        context = {
            'rstate': 1, 
            'containers': containers
        }
        response = aiohttp_jinja2.render_template("dashboard.html", request, context=context)

        return response

    async def get_app(self):
        app = web.Application()
        aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(os.path.join(os.getcwd(), "templates")))
        app.add_routes([web.get('/', self.dashboard), web.get('/config', self.get_config), web.post('/config', self.post_config), web.get('/stopContainer', self.stopContainer), web.get('/startContainer', self.startContainer), web.get('/deleteContainer', self.deleteContainer), web.get('/logging', self.logging)])
        return app